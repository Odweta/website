const express = require('express');

const app = express();
const port = 8080;

app.get('/', (req, res) => {
  res.send('this is a test of my website with express');
});

app.listen(port, (error) => { 
  console.log('Server is Successfully Running, and App is listening on port ' + port); 
});
